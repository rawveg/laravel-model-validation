<?php

namespace Rawveg\ModelValidation\Tests\Helpers\Stubs\Models;

use Illuminate\Database\Eloquent\Model;
use Rawveg\ModelValidation\Concerns\ModelValidation;

/**
 * @method void|false fireModelEvent(string $string)
 */
class TraitNoDefault extends Model
{
    use ModelValidation;
}
