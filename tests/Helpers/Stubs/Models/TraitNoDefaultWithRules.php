<?php

namespace Rawveg\ModelValidation\Tests\Helpers\Stubs\Models;

use Illuminate\Database\Eloquent\Model;
use Rawveg\ModelValidation\Concerns\ModelValidation;
use Rawveg\ModelValidation\Tests\Helpers\Stubs\Rules\ExampleRules;

/**
 * @method void|false fireModelEvent(string $string)
 */
class TraitNoDefaultWithRules extends Model
{
    use ModelValidation;

    /**
     * Create a new Eloquent model instance.
     *
     * @param  array  $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        $this->modelValidationClass = ExampleRules::class;
        parent::__construct($attributes);
    }

}
