<?php

namespace Rawveg\ModelValidation\Tests\Helpers\Stubs\Models;

use Illuminate\Database\Eloquent\Model;
use Rawveg\ModelValidation\Concerns\ModelValidation;

/**
 * @method void|false fireModelEvent(string $string)
 */
class TraitWithDefault extends Model
{
    use ModelValidation;

    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255',
        ];
    }
}
