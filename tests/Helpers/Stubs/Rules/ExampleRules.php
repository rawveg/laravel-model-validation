<?php

namespace Rawveg\ModelValidation\Tests\Helpers\Stubs\Rules;

class ExampleRules
{
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255',
        ];
    }
}

