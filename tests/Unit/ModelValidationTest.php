<?php

use Illuminate\Contracts\Container\BindingResolutionException;
use Rawveg\ModelValidation\Concerns\ModelValidation;

it('has the ModelValidation Concern', function () {
    $this->assertTrue(trait_exists(ModelValidation::class));
});

it('has a model with the ModelValidation concern', function () {
    try {
        $model = getModelWithTraitNoDefault();
    } catch (BindingResolutionException $e) {
        $this->fail($e->getMessage());
    }

    $this->assertTrue(in_array(
        needle: ModelValidation::class,
        haystack: class_uses($model)
    ));
});

it('does not have an overriding rules method', function () {
    try {
        $model = getModelWithTraitNoDefault(mocked: true)->makePartial();
    } catch (BindingResolutionException $e) {
        $this->fail($e->getMessage());
    }

    $this->assertTrue(method_exists($model, 'rules'));
    $this->assertEmpty($model->rules());
});

it('fires the Model::saving event successfully when no default rules set', function () {
    try {
        $model = getModelWithTraitNoDefault(mocked: true)->makePartial();
    } catch (BindingResolutionException $e) {
        $this->fail($e->getMessage());
    }

    $this->assertNotFalse($model->fireModelEvent('saving'));
});

it('has a model with the ModelValidation concern and rules from a validation class', function () {

    $baseRules = [
        "name" => "required|string|max:255",
        "email" => "required|email|max:255"
    ];

    try {
        $model = getModelWithTraitNoDefaultWithRulesClass();
    } catch (BindingResolutionException $e) {
        $this->fail($e->getMessage());
    }

    $this->assertTrue(in_array(
        needle: ModelValidation::class,
        haystack: class_uses($model)
    ));
    $this->assertTrue(method_exists($model, 'rules'));
    $this->assertNotEmpty($model->rules());
    $this->assertEquals($baseRules, $model->rules());
});

it('fires the Model::saving event successfully when rules class is set', function () {
    try {
        $model = getModelWithTraitNoDefaultWithRulesClass(mocked: true)->makePartial();
    } catch (BindingResolutionException $e) {
        $this->fail($e->getMessage());
    }

    $this->assertNotFalse($model->fireModelEvent('saving'));
});

it('has a model with the ModelValidation concern and default rules', function () {

    $baseRules = [
        "name" => "required|string|max:255",
        "email" => "required|email|max:255"
    ];

    try {
        $model = getModelWithRulesMethod();
    } catch (BindingResolutionException $e) {
        $this->fail($e->getMessage());
    }

    $this->assertTrue(in_array(
        needle: ModelValidation::class,
        haystack: class_uses($model)
    ));
    $this->assertTrue(method_exists($model, 'rules'));
    $this->assertNotEmpty($model->rules());
    $this->assertEquals($baseRules, $model->rules());
});

it('fires the Model::saving event successfully when default rules are set', function () {
    try {
        $model = getModelWithRulesMethod(mocked: true)->makePartial();
    } catch (BindingResolutionException $e) {
        $this->fail($e->getMessage());
    }

    $this->assertNotFalse($model->fireModelEvent('saving'));
});
