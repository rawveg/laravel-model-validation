<?php

use Illuminate\Contracts\Container\BindingResolutionException;
use Mockery\LegacyMockInterface;
use Mockery\Mock;
use Rawveg\ModelValidation\Tests\Helpers\Stubs\Models\TraitNoDefault;
use Rawveg\ModelValidation\Tests\Helpers\Stubs\Models\TraitNoDefaultWithRules;
use Rawveg\ModelValidation\Tests\Helpers\Stubs\Models\TraitWithDefault;

/**
 * Get a model with the ModelValidation trait, but with no overriding rules method.
 *
 * @param bool $mocked
 * @return LegacyMockInterface|Mock|TraitNoDefault
 * @throws BindingResolutionException
 */
function getModelWithTraitNoDefault(bool $mocked = false): LegacyMockInterface|Mock|TraitNoDefault
{
    if ($mocked) {
        return \Mockery::mock(TraitNoDefault::class)->makePartial();
    }
    return app()->make(TraitNoDefault::class);
}

/**
 * Get a model with the ModelValidation trait, but with rules imported from a separate validation class.
 *
 * @param bool $mocked
 * @return LegacyMockInterface|Mock|TraitNoDefaultWithRules
 * @throws BindingResolutionException
 */
function getModelWithTraitNoDefaultWithRulesClass(bool $mocked = false): LegacyMockInterface|Mock|TraitNoDefaultWithRules
{
    if ($mocked) {
        return \Mockery::mock(TraitNoDefaultWithRules::class)->makePartial();
    }
    return app()->make(TraitNoDefaultWithRules::class);
}

/**
 * Get a model with the ModelValidation trait, but with default rules.
 *
 * @param bool $mocked
 * @return LegacyMockInterface|Mock|TraitWithDefault
 * @throws BindingResolutionException
 */
function getModelWithRulesMethod(bool $mocked = false): LegacyMockInterface|Mock|TraitWithDefault
{
    if ($mocked) {
        return \Mockery::mock(TraitWithDefault::class)->makePartial();
    }
    return app()->make(TraitWithDefault::class);
}
