<?php

namespace Rawveg\ModelValidation\Validation;

class DefaultValidation
{
    /**
     * Get the validation rules
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }
}
