<?php

namespace Rawveg\ModelValidation\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;

class ModelValidationException extends ValidationException
{
    //
}
