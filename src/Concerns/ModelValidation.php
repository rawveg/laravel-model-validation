<?php

namespace Rawveg\ModelValidation\Concerns;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Rawveg\ModelValidation\Exceptions\ModelValidationException;
use Rawveg\ModelValidation\Validation\DefaultValidation;

/**
 * @method static saving(\Closure $param)
 */
trait ModelValidation
{
    protected string $modelValidationClass = DefaultValidation::class;

    /**
     * Boot the model's validation.
     *
     * @throws ValidationException
     */
    protected static function bootModelValidation(): void
    {
        static::saving(function ($model) {
            if (!empty($model->rules())) {
                $validator = Validator::make($model->attributesToArray(), $model->rules());

                if ($validator->fails()) {
                    throw new ModelValidationException($validator);
                }
            }
        });
    }

    /**
     * Define the model's validation rules.
     * @throws BindingResolutionException
     */
    public function rules(): array
    {
        $rules = [];
        if ($this->modelValidationClass !== null && class_exists($this->modelValidationClass)) {
            $rules = (new $this->modelValidationClass())->rules();
        }
        return $rules;
    }
}
